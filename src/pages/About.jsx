import React from 'react';
import Logo from '../components/Logo';
import Navigations from '../components/Navigations';

const About = () => {
	return (
		<div>
			<Logo/>
			<Navigations/>
			<h1>ABOUT !!</h1>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid non amet eum quisquam! In consequatur tempora sed, et assumenda labore a doloribus pariatur atque modi aliquid accusamus libero quo officiis sit voluptatibus amet adipisci necessitatibus, facilis quis? Assumenda natus deserunt exercitationem laborum sapiente iusto aut magni necessitatibus veniam, ea atque?</p>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid non amet eum quisquam! In consequatur tempora sed, et assumenda labore a doloribus pariatur atque modi aliquid accusamus libero quo officiis sit voluptatibus amet adipisci necessitatibus, facilis quis? Assumenda natus deserunt exercitationem laborum sapiente iusto aut magni necessitatibus veniam, ea atque?</p>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid non amet eum quisquam! In consequatur tempora sed, et assumenda labore a doloribus pariatur atque modi aliquid accusamus libero quo officiis sit voluptatibus amet adipisci necessitatibus, facilis quis? Assumenda natus deserunt exercitationem laborum sapiente iusto aut magni necessitatibus veniam, ea atque?</p>
		</div>
	);
};

export default About;