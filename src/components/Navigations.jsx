import React from 'react';
import {NavLink} from "react-router-dom"

const Navigations = () => {
	return (
		<div className="navigation">
			<ul>
				<li>
					<NavLink
						to="/"
						className={(nav) => nav.isActive ? 'nav-active' : ''}
					>
						Acueil
					</NavLink>
				</li>
				<li>
					<NavLink
						to="/about"
						className={(nav) => nav.isActive ? 'nav-active' : ''}
					>
						A Propos
					</NavLink>
				</li>
			</ul>
		</div>
	);
};

export default Navigations;