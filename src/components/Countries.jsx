import axios from "axios"
import React, { useEffect, useState } from 'react';
import Card from "./Card";

const Countries = () => {

  const [data, setData]                   = useState([])
  const [rangValue, setRangeValue]        = useState(36)
  const [selectedRadio, setSelectedRadio] = useState('')
  const radios = ["Africa", "America", "Asia", "Europe", "Oceania"]

  // Call, mounted components
  useEffect(()=>{
    axios.get('https://restcountries.com/v3.1/all')
      .then(resp => setData(resp.data))
  },[])


  return (
    <div className="countries">
      <h1>Countries</h1>

      <ul className="radio-container">
        <li>
          <input
            type="range" 
            min="1" 
            max={data.filter((country) => country.continents[0].includes(selectedRadio)).length}
            defaultValue={rangValue}
            onChange={(e) => setRangeValue(e.target.value)}
          />
        </li>
        {
          radios.map((contient) => 
            <li key={contient}>
              <input
                id={contient}
                type="radio"
                name="contientRadio"
                onChange={(e) => setSelectedRadio(e.target.id)}
                checked={contient === selectedRadio}
              />
              <label htmlFor={contient}>{contient}</label>
            </li>
          )
        }
      </ul>
      {selectedRadio && (
        <button onClick={() => {setSelectedRadio('')}}>
          Annuler la recherche
        </button>
      )}
      <ul>
        {
          data
            .filter((country) => country.continents[0].includes(selectedRadio))
            .sort((a,b) => b.population - a.population )
            .slice(0, rangValue)
            .map((contry, index) => 
            <Card key={index} country={contry}/>
          )
        }
      </ul>
    </div>
  );
};

export default Countries;